package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class DurationTest {

    @Test
    public void testDur() {
        Duration duration = new Duration();
        String expected = "CP-DOF is designed specifically for corporates and working professionals alike. If you are a corporate and can't dedicate full day for training, then you can opt for either half days course or  full days programs which is followed by theory and practical exams.";
        assertEquals(expected, duration.dur());
    }
    
    @Test
    public void testCalculateIntValue() {
        Duration duration = new Duration();
        duration.calculateIntValue();  // This will pass if the method doesn't throw an exception
    }

}
